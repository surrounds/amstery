<?php

use yii\db\Migration;

/**
 * Class m171211_210918_insert_setting_groups
 */
class m171211_210918_insert_setting_groups extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->insert('setting_groups',[
			'name' => 'general',
		]);
		$this->insert('setting_groups',[
			'name' => 'contacts',
		]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('setting_groups');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_210918_insert_setting_groups cannot be reverted.\n";

        return false;
    }
    */
}
