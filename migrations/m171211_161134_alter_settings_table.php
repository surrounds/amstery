<?php

use yii\db\Migration;

/**
 * Class m171211_161134_alter_settings_table
 */
class m171211_161134_alter_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->addColumn('settings', 'group', $this->integer()->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('settings', 'group');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_161134_alter_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
