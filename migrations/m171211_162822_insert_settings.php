<?php

use yii\db\Migration;

/**
 * Class m171211_160822_insert_settings
 */
class m171211_162822_insert_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->insert('settings',[
			'title' => 'Phone',
			'name' => 'contacts_phone',
			'value' => '+380 63 808 65 82',
			'group' => 2,
		]);
		$this->insert('settings',[
			'title' => 'Email',
			'name' => 'contacts_email',
			'value' => 'info@amstery.com',
			'group' => 2,
		]);
		$this->insert('settings',[
			'title' => 'Skype',
			'name' => 'contacts_skype',
			'value' => 'amstery.studio',
			'group' => 2,
		]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
			
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_160822_insert_settings cannot be reverted.\n";

        return false;
    }
    */
}
