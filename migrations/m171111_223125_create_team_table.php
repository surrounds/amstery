<?php

use yii\db\Migration;

/**
 * Handles the creation of table `team`.
 */
class m171111_223125_create_team_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('team', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'job' => $this->string(),
            'skills' => $this->string(),
            'review' => $this->string(),
            'about' => $this->text(),
						'image' => $this->string(),
						'mt_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('team');
    }
}
