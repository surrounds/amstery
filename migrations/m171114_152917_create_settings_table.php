<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m171114_152917_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
						'title' => $this->string(),
						'name' => $this->string(),
						'value' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
