<?php

use yii\db\Migration;

/**
 * Class m171114_161142_insert_settings_table
 */
class m171114_161142_insert_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
				$this->insert('settings',[
					'title' => 'Wellcome first line',
					'name' => 'wellcome_first_line',
					'value' => 'Welcome To Our Studio!',
				]);
			
				$this->insert('settings',[
					'title' => 'Wellcome second line',
					'name' => 'wellcome_second_line',
					'value' => 'IT\'S NICE TO MEET YOU',
				]);
			
				$this->insert('settings',[
					'title' => 'Phone',
					'name' => 'phone',
					'value' => '+1 555 222 11 44',
				]);
			
				$this->insert('settings',[
					'title' => 'Email',
					'name' => 'email',
					'value' => 'info@amstery.com',
				]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171114_161142_insert_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
