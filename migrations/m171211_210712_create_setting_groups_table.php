<?php

use yii\db\Migration;

/**
 * Class m171211_210712_alter_setting_groups_table
 */
class m171211_210712_create_setting_groups_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('setting_groups', [
            'id' => $this->primaryKey(),
			'name' => $this->string(),
			'order' => $this->integer(1)->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('setting_groups');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_210712_alter_setting_groups_table cannot be reverted.\n";

        return false;
    }
    */
}
