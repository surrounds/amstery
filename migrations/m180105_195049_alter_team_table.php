<?php

use yii\db\Migration;

/**
 * Class m180105_195049_alter_team_table
 */
class m180105_195049_alter_team_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
			$this->addColumn('team', 'url', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('team', 'url');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180105_195049_alter_team_table cannot be reverted.\n";

        return false;
    }
    */
}
