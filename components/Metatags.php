<?php

namespace app\components;

use Yii;
use app\models\Metatags as Metatag;

class Metatags
{
	public static function getMetatags($id)
	{
			$metatags = Metatag::findOne($id);
		
			if ($metatags) {
				if ($metatags->meta_title) {
					\Yii::$app->view->registerMetaTag([
							'name' => 'title',
							'content' => $metatags->meta_title
					]);
				}
				if ($metatags->meta_desctiption) {
					\Yii::$app->view->registerMetaTag([
							'name' => 'description',
							'content' => $metatags->meta_desctiption
					]);
				}
				if ($metatags->meta_keywords) {
					\Yii::$app->view->registerMetaTag([
							'name' => 'keywords',
							'content' => $metatags->meta_keywords
					]);
				}
				if ($metatags->ogt_title) {
					\Yii::$app->view->registerMetaTag([
							'property' => 'og:title',
							'content' => $metatags->ogt_title
					]);
				}
				if ($metatags->ogt_description) {
					\Yii::$app->view->registerMetaTag([
							'property' => 'og:description',
							'content' => $metatags->ogt_description
					]);
				}
				if ($metatags->ogt_image) {
					\Yii::$app->view->registerMetaTag([
							'property' => 'og:image',
							'content' => '/'.$metatags->ogt_image
					]);
				}
			}
	}

	public static function getMetatagsByUrl($url = false)
	{
			if(count(\Yii::$app->view->metaTags) > 0)
				return;
		
			if(!$url) {
				$fullUrl = explode('/', Yii::$app->request->url);
				$url = end($fullUrl);	
				
			}
			$metatags = Metatag::find()->where(['url' => $url])->one();
			//var_dump($metatags);exit;
			if ($metatags) {
				if ($metatags->meta_title) {
					\Yii::$app->view->registerMetaTag([
							'name' => 'title',
							'content' => $metatags->meta_title
					]);
				}
				if ($metatags->meta_desctiption) {
					\Yii::$app->view->registerMetaTag([
							'name' => 'description',
							'content' => $metatags->meta_desctiption
					]);
				}
				if ($metatags->meta_keywords) {
					\Yii::$app->view->registerMetaTag([
							'name' => 'keywords',
							'content' => $metatags->meta_keywords
					]);
				}
				if ($metatags->ogt_title) {
					\Yii::$app->view->registerMetaTag([
							'property' => 'og:title',
							'content' => $metatags->ogt_title
					]);
				}
				if ($metatags->ogt_description) {
					\Yii::$app->view->registerMetaTag([
							'property' => 'og:description',
							'content' => $metatags->ogt_description
					]);
				}
				if ($metatags->ogt_image) {
					\Yii::$app->view->registerMetaTag([
							'property' => 'og:image',
							'content' => '/'.$metatags->ogt_image
					]);
				}
			}
	}
}