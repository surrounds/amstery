<?php

namespace app\components;

use Yii;
use yii\base\Widget;
use app\models\Menu;

class MenuWidget extends Widget
{
	public $tags;
	private $renderedMenu;
	
	public function init(){
		parent::init();
		
		if (!$this->renderedMenu) {
			$this->renderedMenu = '';
		}
		
		$allMenu = Menu::find()->orderBy('order')->all();
		if(!$allMenu){
			$this->renderedMenu= '';
		}else{
			foreach ($allMenu as $menu)
				$this->renderedMenu .= '<li><a href="' . $menu->url . '">' . $menu->title . '</a></li>';
		}
		$this->local();
	}
	
	protected function local()
	{
		$this->renderedMenu .= '<li class="change-language">';
		
        if(Yii::$app->language == 'en-US') {
        	$this->renderedMenu .= '<a class="pull-left clearfix" href="/ru">' . Yii::t('app','RU') . '</a>';
        } else {
         	$this->renderedMenu .= '<a class="pull-left clearfix" href="/">' . Yii::t('app','EN') . '</a>';
        }
		$this->renderedMenu .= '</li>';
	}
	
	public function run(){
		return $this->renderedMenu;
	}	
}
