<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?php echo Yii::t('app', 'Admin Panel')?> | <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <?php NavBar::begin([
        'brandLabel' => 'Amstery Admin Panel',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);?>
    <?php if(!Yii::$app->user->isGuest): ?>
    <?php echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => Yii::t('app','Pages'), 'url' => ['/admin/pages/index']],
            ['label'=> Yii::t('app','Team'), 'url' => ['/admin/team/index']],
            ['label' => Yii::t('app','Blog'), 'url' => ['/admin/blog/index']],
            ['label' => Yii::t('app','Services'), 'url' => ['/admin/services/index']],
            ['label' => Yii::t('app','MetaTags'), 'url' => ['/admin/metatags/index']],
            ['label' => Yii::t('app','Settings'), 'items'=>[
								['label'=> Yii::t('app','Edit Settings'), 'url' => ['/admin/settings/edit']],
                ['label'=> Yii::t('app','Add setting'), 'url' => ['/admin/settings/create']],
                ['label'=> Yii::t('app','Show all'), 'url' => ['/admin/settings/index']],
            ]],						
            ['label' => Yii::t('app', 'Logout'), 'url'=> ['/site/logout']],
        ],
    ]);?>
    <?php endif; ?>
    <?php NavBar::end();?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Amstery <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
