<?php

use yii\widgets\ActiveForm;
use sjaakp\sortable\SortableGridView;
Yii::$app->controller->enableCsrfValidation = false;
?>

<div id="menu">
	<?php $form = ActiveForm::begin(); 
	echo SortableGridView::widget( [
			'dataProvider' => $dataProvider,
			'orderUrl' => ['order'],
			'columns' => [
					[
						'format' => 'raw',
						'attribute' => 'title',
						'value' => function($data) {return $this->render('menuRow',['id' => $data->id, 'title' => $data->title, 'type' => 'title']);}
					],
					[
						'format' => 'raw',
						'attribute' => 'url',
						'value' => function($data) {return $this->render('menuRow',['id' => $data->id, 'title' => $data->url, 'type' => 'url']);}
					],
			],
	] );
	
	ActiveForm::end();
	?>
</div>

