<?php foreach($settings as $setting): ?>
				
	<div class="form-group">
		<div class="col-md-2">
			<label class="control-label" for="settings-value"><?=$setting->title?></label>						
		</div>
		<div class="col-md-10">
			<input type="text" class="form-control" name="setting[<?=$setting->id?>]" value="<?=$setting->value?>">
		</div>
	</div>

<?php endforeach; ?>