<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="services-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Services', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
						[
							'format' => 'raw',
							'attribute' => 'icon',
							'value' => function($data){return '<span id="selected-icon">'. ($data->icon ? "<i class='".$data->icon."'></i>" : '') . '</span>';}
						],
            'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
</div>
