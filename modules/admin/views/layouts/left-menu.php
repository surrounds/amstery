<?php 

namespace app\modules\admin\views\layouts;

use Yii;
use app\models\ContactForm;

$unreadContact = ContactForm::find()->where(['read' => 0])->count();

?>   

   <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'pages') echo " active" ?>">
          <a href="#">
            <i class="fa fa-list-ul"></i>
            <span><?= Yii::t('app','Pages')?></span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/pages/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Pages')?></a></li>
            <li><a href="/admin/pages/create"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Add Page')?></a></li>
          </ul>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'team') echo " active" ?>">
          <a href="#">
            <i class="fa fa-users"></i>
            <span><?= Yii::t('app','Team')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/team/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Team')?></a></li>
            <li><a href="/admin/team/create"><i class="fa fa-user-plus"></i> <?= Yii::t('app','Add Teammate')?></a></li>
          </ul>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'blog') echo " active" ?>">
          <a href="#">
            <i class="fa fa-file-text-o"></i>
            <span><?= Yii::t('app','Blog')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/blog/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Blog')?></a></li>
            <li><a href="/admin/blog/create"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Add Article')?></a></li>
          </ul>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'services') echo " active" ?>">
          <a href="#">
            <i class="fa fa-gears"></i>
            <span><?= Yii::t('app','Services')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/services/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Services')?></a></li>
            <li><a href="/admin/services/create"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Add Service')?></a></li>
          </ul>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'careers') echo " active" ?>">
          <a href="#">
            <i class="fa fa-gears"></i>
            <span><?= Yii::t('app','Careers')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/careers/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Careers')?></a></li>
            <li><a href="/admin/careers/create"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Add Career')?></a></li>
          </ul>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'portfolio') echo " active" ?>">
          <a href="#">
            <i class="fa fa-picture-o"></i>
            <span><?= Yii::t('app','Portfolio')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/portfolio/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Portfolio')?></a></li>
            <li><a href="/admin/portfolio/create"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Add Portfolio')?></a></li>
          </ul>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'contacts') echo " active" ?>">
          <a href="/admin/contacts/index">
            <i class="fa fa-phone"></i>
            <span><?= Yii::t('app','Contacts')?></span>
            <?php if($unreadContact > 0): ?>            	
							<span class="pull-right-container">
								<span class="label label-danger pull-right"><?=$unreadContact?></span>
							</span>
            <?php endif; ?>
          </a>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'menu') echo " active" ?>">
          <a href="/admin/menu/index">
            <i class="fa fa-list-ul"></i>
            <span><?= Yii::t('app','Menu')?></span>
          </a>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'metatags') echo " active" ?>">
          <a href="#">
            <i class="fa fa-list-ul"></i>
            <span><?= Yii::t('app','Metatags')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/metatags/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Metatags')?></a></li>
            <li><a href="/admin/metatags/create"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Add Metatag')?></a></li>
          </ul>
        </li>
        <li class="treeview<?php if(Yii::$app->controller->id == 'settings') echo " active" ?>">
          <a href="#">
            <i class="fa fa-gears"></i>
            <span><?= Yii::t('app','Settings')?></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/admin/settings/edit"><i class="fa fa-circle-o"></i> <?= Yii::t('app','Edit Settings')?></a></li>
            <li><a href="/admin/settings/index"><i class="fa fa-circle-o"></i> <?= Yii::t('app','View Settings')?></a></li>
            <li><a href="/admin/settings/create"><i class="fa fa-gear"></i> <?= Yii::t('app','Add Setting')?></a></li>
          </ul>
        </li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>