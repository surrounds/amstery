<?php

namespace app\modules\admin\views;

use yii\helpers\Html;

$totalMessages = count($model);

?>
<section class="content-header">
	<h1>
		Mailbox
		<small><?=$totalMessages?> new messages</small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Mailbox</li>
	</ol>
</section>

<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Inbox</h3>

						<div class="box-tools pull-right">
							<div class="has-feedback">
								<input type="text" class="form-control input-sm" placeholder="Search Mail">
								<span class="glyphicon glyphicon-search form-control-feedback"></span>
							</div>
						</div>
						<!-- /.box-tools -->
					</div>
					<!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="mailbox-controls">
							<!-- Check all button -->
							<button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
							</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
								<button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
								<button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
							</div>
							<!-- /.btn-group -->
							<button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
							<div class="pull-right">
								1-50/200
								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
								</div>
								<!-- /.btn-group -->
							</div>
							<!-- /.pull-right -->
						</div>
						<div class="table-responsive mailbox-messages">
							<table class="table table-hover table-striped">
								<tbody>
									<?php foreach($model as $contact): ?>										
										<tr class="<?= $contact->read ? "success" : "danger" ?>">
											<td><input type="checkbox"></td>
											<td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>
											<td class="mailbox-name"><?=Html::a($contact->name, ['/admin/contacts/view', 'id' => $contact->id])?></td>
											<td class="mailbox-name"><?=Html::a($contact->email, ['/admin/contacts/view', 'id' => $contact->id])?></td>
											
											<td class="mailbox-subject">
												<?= strlen($contact->message) > 50 ? (substr($contact->message,0,50).'...') : $contact->message?>
											</td>
											<td class="mailbox-date"><?=date('d-m-Y H:i:s', $contact->created)?></td>
										</tr>
									<?php endforeach; ?>
								</tbody>
							</table>
							<!-- /.table -->
						</div>
						<!-- /.mail-box-messages -->
					</div>
					<!-- /.box-body -->
					<div class="box-footer no-padding">
						<div class="mailbox-controls">
							<!-- Check all button -->
							<button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i>
							</button>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-sm"><i class="fa fa-trash-o"></i></button>
								<button type="button" class="btn btn-default btn-sm"><i class="fa fa-reply"></i></button>
								<button type="button" class="btn btn-default btn-sm"><i class="fa fa-share"></i></button>
							</div>
							<!-- /.btn-group -->
							<button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
							<div class="pull-right">
								1-50/200
								<div class="btn-group">
									<button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
								</div>
								<!-- /.btn-group -->
							</div>
							<!-- /.pull-right -->
						</div>
					</div>
				</div>
				<!-- /. box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
</section>
