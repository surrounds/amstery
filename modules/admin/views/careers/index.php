<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Careers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="careers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'New Career'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'url:url',
						[
							'format' => 'raw',
							'attribute' => 'icon',
							'value' => function($data){return '<span id="selected-icon">'. ($data->icon ? "<i class='".$data->icon."'></i>" : '') . '</span>';}
						],
            //'text:ntext',
            //'meta_title',
            // 'meta_desctiption',
            // 'meta_keywords',
            // 'ogt_title',
            // 'ogt_description',
            // 'ogt_image',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
