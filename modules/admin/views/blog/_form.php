<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Blogtag;
/* @var $this yii\web\View */
/* @var $model app\models\Blog */
/* @var $form yii\widgets\ActiveForm */
?>
<ul class="nav nav-tabs nav-pills" id="menu_tabs" role="tablist">
    <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Main</a></li>
    <li role="presentation"><a href="#metatags" aria-controls="metatags" role="tab" data-toggle="tab">Meta tags</a></li>
</ul>

<div class="blog-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="main">
				<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
				
				<?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
				
				<?= $form->field($model, 'file')->fileInput(['maxlength' => true]) ?>
				
				<?php if($model->img): ?>
					<div class="form-group field-pages-ogt_description">
					<img src="/img/blog/<?=$model->img?>" width="300px">
				</div>
				<?php endif; ?>
		
				<div class="form-group field-blog-short_desc">
					<label class="control-label" for="blog-short_desc">Short Description</label>
					<textarea id="blog-short_desc" class="form-control" name="Blog[short_desc]" rows="6" aria-invalid="false"><?=$model->short_desc?></textarea>
				</div>
		
				<div class="form-group field-blog-full_desc">
					<label class="control-label" for="blog-full_desc">Full Description</label>
					<textarea id="blog-full_desc" class="form-control" name="Blog[full_desc]" rows="6" aria-invalid="false"><?=$model->full_desc?></textarea>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="metatags">
				<?= $form->field($mtModel, 'meta_title')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'meta_desctiption')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'meta_keywords')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'ogt_title')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'ogt_description')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'image')->fileInput() ?>

				<?php if($mtModel->ogt_image): ?>
					<div class="form-group field-pages-ogt_description">
					<img src="/<?=$mtModel->ogt_image?>" width="300px">
				</div>
				<?php endif; ?>
			</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

