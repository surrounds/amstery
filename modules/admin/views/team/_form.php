<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */
?>
<ul class="nav nav-tabs nav-pills" id="menu_tabs" role="tablist">
    <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Main</a></li>
    <li role="presentation"><a href="#metatags" aria-controls="metatags" role="tab" data-toggle="tab">Meta tags</a></li>
</ul>
<div class="team-form">
	
    <?php $form = ActiveForm::begin(); ?>
    <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="main">
				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'job')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'skills')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

				<?= $form->field($model, 'fileImage')->fileInput() ?>

				<?php if($model->image): ?>
					<div class="form-group field-pages-image">
					<img src="/<?=$model->image?>" width="300px">
				</div>
				<?php endif; ?>

				<?= $form->field($model, 'review')->textInput(['maxlength' => true]) ?>
		
				<div class="form-group field-team-about">
					<label class="control-label" for="team-about">About</label>
					<textarea id="team-about" class="form-control" name="Team[about]" rows="6" aria-invalid="false"><?=$model->about?></textarea>
				</div>
			</div>
    
			<div role="tabpanel" class="tab-pane" id="metatags">
				<?= $form->field($mtModel, 'meta_title')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'meta_desctiption')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'meta_keywords')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'ogt_title')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'ogt_description')->textInput(['maxlength' => true]) ?>

				<?= $form->field($mtModel, 'image')->fileInput() ?>

				<?php if($mtModel->ogt_image): ?>
					<div class="form-group field-pages-ogt_description">
					<img src="/<?=$mtModel->ogt_image?>" width="300px">
				</div>
				<?php endif; ?>
			</div>
  </div>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

