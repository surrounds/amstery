<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Teams'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'job',
            'skills',
            'review',
						'url',
            'about:html',
            [
                'attribute'=>'image',
                'value'=>Yii::getAlias("@web/".$model->image),
                'format'=>'image'
            ],
        ],
    ]) ?>
    
    <h3>Meta tags</h3>
    <?= DetailView::widget([
        'model' => $mtModel,
        'attributes' => [
            'meta_title',
            'meta_desctiption',
            'meta_keywords',
            'ogt_title',
            'ogt_description',
						[
							'format' => 'raw',
							'attribute' => 'ogt_image',
							'value' => '<image src="/'.$mtModel->ogt_image.'" width="300px">'
						],
        ],
    ]) ?>

</div>
