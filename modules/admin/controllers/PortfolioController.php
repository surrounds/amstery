<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Portfolio;
use app\models\Team;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PortfolioController implements the CRUD actions for Portfolio model.
 */
class PortfolioController extends Controller
{
    /**
     * Lists all Portfolio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Portfolio::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Portfolio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Portfolio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Portfolio();
				$workers = $this->getTeamSelect();

        if ($model->load(Yii::$app->request->post())) {
					if (is_array($model->workers)) {
						$model->workers = implode(',',$model->workers);
					}
					if ($model->started) {
						$model->started = strtotime($model->started);
					}
					if ($model->finished) {
						$model->finished = strtotime($model->finished);
					}
					
					$model->order = (int)$model->order;
					
					if($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
					} else {
							return $this->render('update', [
									'model' => $model,
									'workers' => $workers,
							]);
					}
        } else {
            return $this->render('create', [
                'model' => $model,
								'workers' => $workers,
            ]);
        }
    }
	
		private function getTeamSelect($workers = null)
		{
				if ($workers) {
					$workers = explode(',', $workers);				
				}
				$users = Team::find()->all();
				$select = '';
				foreach ($users as $user) {
					if ($workers) {
						$selected = (array_search((string)$user->id, $workers) !== false) ? ' selected' : '';
						$select .= '<option value="'.$user->id.'"'.$selected.'>'.$user->name.'</option>';
					} else {
						$select .= '<option value="'.$user->id.'">'.$user->name.'</option>';
					}
				}
				return $select;			
		}

    /**
     * Updates an existing Portfolio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
				$workers = $this->getTeamSelect($model->workers);
			
				if ($model->started) {
					$model->started = date('M Y', $model->started);
				}
			
				if ($model->finished) {
					$model->finished = date('M Y', $model->finished);
				}

        if ($model->load(Yii::$app->request->post())) {
					if (is_array($model->workers)) {
						$model->workers = implode(',',$model->workers);
					}
					if ($model->started) {
						$model->started = strtotime($model->started);
					}
					if ($model->finished) {
						$model->finished = strtotime($model->finished);
					}					
					
					if($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
					} else {
							return $this->render('update', [
									'model' => $model,
									'workers' => $workers,
							]);
					}
        } else {
            return $this->render('update', [
                'model' => $model,
								'workers' => $workers,
            ]);
        }
    }

    /**
     * Deletes an existing Portfolio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Portfolio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Portfolio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Portfolio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
