<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Careers;
use app\models\Metatags;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * PagesController implements the CRUD actions for Careers model.
 */
class CareersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Careers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Careers::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Careers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
				$mtModel = Metatags::findOne($model->mt_id);
			
        return $this->render('view', [
            'model' => $model,
						'mtModel' => $mtModel
        ]);
    }

    /**
     * Creates a new Careers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Careers();
				$mtModel = new Metatags();

        if ($mtModel->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $mtModel->save()) {
						$model->mt_id = $mtModel->id;
						if ($model->save()) {
							$mtModel->image = UploadedFile::getInstance($mtModel, 'image');
							$mtModel->url = $model->url;
							if ($mtModel->image) {
								if ($filename = $mtModel->upload('careers')) {
										$mtModel->ogt_image = $filename;
										$mtModel->save();
								}
							}
							return $this->redirect(['view', 'id' => $model->id]);
						}
        } else {
            return $this->render('create', [
                'model' => $model,
								'mtModel' => $mtModel
            ]);
        }
    }

    /**
     * Updates an existing Careers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
				$mtModel = Metatags::findOne($model->mt_id);

        if ($mtModel->load(Yii::$app->request->post()) && $model->load(Yii::$app->request->post()) && $model->save()) {
					$mtModel->url = $model->url;
					$mtModel->save();
					$mtModel->image = UploadedFile::getInstance($mtModel, 'image');
						if ($mtModel->image) {
							if ($filename = $mtModel->upload('careers')) {
									$mtModel->ogt_image = $filename;
									$mtModel->save();
							}
						}
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
								'mtModel' => $mtModel
            ]);
        }
    }

    /**
     * Deletes an existing Careers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
				$mtModel = Metatags::findOne($model->mt_id);
				$model->delete();
				$mtModel->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Careers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Careers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Careers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
