<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Blog;
use app\models\UploadForm;
use app\models\Metatags;
use yii\data\ActiveDataProvider;
use app\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BlogController implements the CRUD actions for Blog model.
 */
class BlogController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Blog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Blog::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Blog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
				$mtModel = Metatags::findOne($model->mt_id);
			
        return $this->render('view', [
            'model' => $model,
						'mtModel' => $mtModel
        ]);
    }

    /**
     * Creates a new Blog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new Blog();
		$fmodel = new UploadForm('img/blog');
		$mtModel = new Metatags();

		if (Yii::$app->request->isPost) {
			$fmodel->src = UploadedFile::getInstance($model, 'file');
			$model->load(Yii::$app->request->post());
			$model->img = (string)UploadedFile::getInstance($model, 'file');
			$model->datetime = (string)time();
		
			$mtModel->load(Yii::$app->request->post());					
			$mtModel->image = UploadedFile::getInstance($mtModel, 'image');
			$mtModel->url = $model->url;
			if ($mtModel->image) {
				if ($filename = $mtModel->upload('team')) {
						$mtModel->ogt_image = $filename;
				}
			}
			$mtModel->save();
			$model->mt_id = $mtModel->id;

			if ($fmodel->upload()) {
				$model->file=null;
			}
			if($model->save()){
				return $this->redirect(['view', 'id' => $model->id]);
			}
		}

        return $this->render('create', ['model' => $model, 'mtModel' => $mtModel]);

    }

    /**
     * Updates an existing Blog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
				$model = $this->findModel($id);
				$fmodel = new UploadForm('img/blog');
				$mtModel = Metatags::findOne($model->mt_id);

				if (Yii::$app->request->isPost) {
						$mtModel->load(Yii::$app->request->post());					
						$mtModel->image = UploadedFile::getInstance($mtModel, 'image');
						$mtModel->url = $model->url;
						if ($mtModel->image) {
							if ($filename = $mtModel->upload('team')) {
									$mtModel->ogt_image = $filename;
							}
						}
						$mtModel->save();
					
						$fileSrc = $model->img;
						$fmodel->src = UploadedFile::getInstance($model, 'file');
						$model->load(Yii::$app->request->post());
						$model->img = (string)UploadedFile::getInstance($model, 'file');

						if(strlen($model->img) < 1)
							$model->img = $fileSrc;

						if($fmodel->src !== NULL && strlen($fmodel->src) > 0)
							$fmodel->upload();
							
							$model->file=null;
						if($model->save()){
							return $this->redirect(['view', 'id' => $model->id]);


							return $this->render('update', ['model' => $model, 'mtModel' => $mtModel]);
						}
				}

        return $this->render('update', ['model' => $model, 'mtModel' => $mtModel]);

    }

    /**
     * Deletes an existing Blog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
	
		public function actionGenerateUrl($title)
		{
				$title = strtolower($title);
				$title = str_replace(" ", "-", $title);
				$title = str_replace("(", "", $title);
				$title = str_replace(")", "", $title);
				$title = str_replace("!", "", $title);
				$title = str_replace("?", "", $title);
				$title = str_replace(":", "", $title);
				$title = str_replace(",", "", $title);
			
				echo $title;
		}

    /**
     * Finds the Blog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Blog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Blog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
