<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property string $title_ru
 * @property string $title_en
 * @property string $src
 * @property string $datetime
 * @property string $short_desc_ru
 * @property string $short_desc_en
 * @property string $full_desc_ru
 * @property string $full_desc_en
 */
class Blog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
		public $file;
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url', 'short_desc', 'full_desc'], 'required'],
            [['short_desc', 'full_desc'], 'string'],
						[['mt_id'], 'integer'],
            [['title', 'url', 'img', 'datetime'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
						'img' => 'Image',
            'datetime' => 'Date',
            'short_desc' => 'Short Desc Ru',
            'full_desc' => 'Full Desc En',
        ];
    }
}
