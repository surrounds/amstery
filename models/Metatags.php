<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "metatags".
 *
 * @property integer $id
 * @property string $meta_title
 * @property string $meta_desctiption
 * @property string $meta_keywords
 * @property string $ogt_title
 * @property string $ogt_description
 * @property string $ogt_image
 */
class Metatags extends \yii\db\ActiveRecord
{
		public $image;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'metatags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'meta_title', 'meta_desctiption', 'meta_keywords', 'ogt_title', 'ogt_description', 'ogt_image'], 'string', 'max' => 255],
        ];
    }
	
		public function upload($folder = 'pages')
    {
        if ($this->validate()) { 
						$filename = 'img/'.$folder.'/' . $this->image->baseName . '.' . $this->image->extension;
            $this->image->saveAs($filename);
						$this->image = null;
            return $filename;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_desctiption' => Yii::t('app', 'Meta Desctiption'),
            'meta_keywords' => Yii::t('app', 'Meta Keywords'),
            'ogt_title' => Yii::t('app', 'Ogt Title'),
            'ogt_description' => Yii::t('app', 'Ogt Description'),
            'ogt_image' => Yii::t('app', 'Ogt Image'),
        ];
    }
}
