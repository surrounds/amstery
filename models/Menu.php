<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property integer $order
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }
	
		public function behaviors( ) {
				return [
						[
								'class' => 'sjaakp\sortable\Sortable',
						],
				];
		}
	
		public function formatInputLabel()
		{
			return '<div class="form-group has-success has-feedback">
  <input type="text" class="form-control editMenuInput"  name="menu['.$data->id.']" value="'.$data->title.'" aria-describedby="inputSuccess2Status">
  <span class="glyphicon glyphicon-ok form-control-feedback" aria-hidden="true"></span>
  <span id="inputSuccess2Status" class="sr-only">(success)</span>
</div>';
		}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'url'], 'required'],
            [['order'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'url' => 'Url',
            'order' => 'Order',
        ];
    }
}
