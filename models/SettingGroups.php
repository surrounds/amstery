<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting_groups".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order
 */
class SettingGroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
	
		public function getTabId()
		{
			return str_replace(' ', '-', strtolower($this->name));
		}

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'order' => 'Order',
        ];
    }
}
