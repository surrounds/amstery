<section>
	<div class="container">
		<div class="row">
			<h3><?=$model->title?></h3>
			
			<div class="bg-gray">
				<div class="blog-detail-image">
					<img src="/img/blog/<?=$model->img?>" alt="<?=$model->title?>" width="100%">
				</div>
				<?=$model->full_desc?>
			</div>
		</div>
	</div>
</section>