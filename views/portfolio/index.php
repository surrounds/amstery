<?php

/* @var $this yii\web\View */
?>
    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><?=Yii::t('app','Portfolio')?></h2>
                    <h3 class="section-subheading text-muted"><?=Yii::t('app','Some our works')?>.</h3>
                </div>
            </div>
            <div class="row">
							<?php foreach ($model as $pf): ?>
								<div class="col-md-4 col-sm-6 portfolio-item">
										<a href="#portfolioModal<?=$pf->id?>" class="portfolio-link" data-toggle="modal">
												<div class="portfolio-hover">
														<div class="portfolio-hover-content">
																<i class="fa fa-plus fa-3x"></i>
														</div>
												</div>
												<img src="<?=$pf->icon?>" class="img-responsive" alt="">
										</a>
										<div class="portfolio-caption">
												<h4><?=$pf->name?></h4>
										</div>
								</div>
								
								
							<?php endforeach; ?>
            </div>
        </div>
    </section>


		<?php foreach ($model as $pf): ?>
			<div class="portfolio-modal modal fade" id="portfolioModal<?=$pf->id?>" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
							<div class="modal-content">
									<div class="close-modal" data-dismiss="modal">
											<div class="lr">
													<div class="rl">
													</div>
											</div>
									</div>
									<div class="container">
											<div class="row">
													<div class="col-lg-8 col-lg-offset-2">
															<div class="modal-body">
																	<h2><?=$pf->name?></h2>
																	<div>
																		<?=$pf->about?>
																	</div>
																	<div>
																		<?=$pf->wedid?>
																	</div>
																	<div>
																		<?=$pf->prettyWorkers?>
																	</div>
																	<div></div>
																	<p>
																			<strong>Want these icons in this portfolio item sample?</strong>
																			You can download 60 of them for free, courtesy of 
																			<a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">RoundIcons.com</a>
																			, or you can purchase the 1500 icon set <a href="https://getdpd.com/cart/hoplink/18076?referrer=bvbo4kax5k8ogc">here</a>.
																	</p>
																	<ul class="list-inline">
																			<li>Date: <?=$pf->period?></li>
																	</ul>
																	<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
															</div>
													</div>
											</div>
									</div>
							</div>
					</div>
			</div>
		<?php endforeach; ?>
