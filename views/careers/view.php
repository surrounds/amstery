<?php
/* @var $this yii\web\View */
?>
<section id="<?=$model->url?>" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-heading text-center"><?=$model->title?></h1>
                <?=$model->text?>
            </div>
        </div>
    </div>
</section>
