<?php
/* @var $this yii\web\View */
?>
<section id="careers" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Our Opened Position</h2>
                <h3 class="section-subheading text-muted">We're looking for you, our ideal candidate.</h3>
                <?php foreach($model as $career): ?>
									<div class="col-sm-4">
										<div class="careers-member">
                        <a href="/careers/<?=$career->url?>">													
													<span class="fa-stack">
															<i class="fa fa-circle fa-stack-2x text-primary"></i>
															<i class="<?=$career->icon?>"></i>
													</span>
													<h4 class="service-heading"><?=$career->title?></h4>
                   			</a>
                    </div>
									</div>
           			<?php endforeach; ?>
            </div>
        </div>
    </div>
</section>
