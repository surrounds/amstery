<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contacts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
   <section id="contacts">
   		<div class="container">
   			<div class="row">
					<div class="col-md-12 text-center">
						<h1><?= Html::encode($this->title) ?></h1>
					</div>
					<div class="col-md-12">
							<div class="col-md-6">
								<?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>
										<div class="alert alert-success">
												Thank you for contacting us. We will respond to you as soon as possible.
										</div>
								<?php else: ?>
										<h3>Contact Form</h3>
										<div class="row">
												<div class="col-md-12">
														<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
																<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
																<?= $form->field($model, 'email') ?>
																<?= $form->field($model, 'subject') ?>
																<?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
																<?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
																		'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
																]) ?>
																<div class="form-group">
																		<?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
																</div>
														<?php ActiveForm::end(); ?>
												</div>
										</div>
								<?php endif; ?>
							</div>
							<div class="col-md-6">
								<h3>Other channels</h3>
								<div class="row">
									<?php foreach ($contacts as $contact): ?>
										<div class="col-md-12">
											<div class="col-md-6">
												<h4><?=ucfirst($contact->title)?></h4>
											</div>
											<div class="col-md-6">
												<h4><?=$contact->value?></h4>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
					</div>
   			</div>
   		</div>
   </section>

    
</div>
