<div class="col-md-4">
		<span class="fa-stack fa-4x">
				<i class="fa fa-circle fa-stack-2x text-primary"></i>
				<i class="<?=$icon?>"></i>
		</span>
		<h4 class="service-heading"><?=$title?></h4>
		<p class="text-muted"><?=$description?></p>
</div>