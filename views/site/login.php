<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
							<h1><?= Html::encode($this->title) ?></h1>

							<p>Please fill out the following fields to login:</p>

							<?php $form = ActiveForm::begin([
									'id' => 'login-form',
									'layout' => 'horizontal',
							]); ?>
								<div class="col-md-6 col-md-offset-3">
									<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
								</div>
								<div class="col-md-6 col-md-offset-3">
									<?= $form->field($model, 'password')->passwordInput() ?>
								</div>
								<div class="col-md-6 col-md-offset-3">
									<?= $form->field($model, 'rememberMe')->checkbox([
											'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
									]) ?>
								</div>

									<div class="col-md-6 col-md-offset-3">
											<div class="col-lg-offset-1 col-lg-11">
													<?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
											</div>
									</div>

							<?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>