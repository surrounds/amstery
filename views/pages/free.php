<?php 
	use yii\helpers\Html;
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-W5ZXRVG');</script>
		<!-- End Google Tag Manager -->
		<title>Amstery - Free</title>

		<meta name="Keywords" content="Amstery, free, investment, инвестиции, бесплатный сайт">
		<meta name="Description" content="">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	
		<?= Html::csrfMetaTags() ?>
		<!-- Favicon for Desktop, iOS and android -->
		<link rel="apple-touch-icon" sizes="57x57" href="/frees/images/favicons/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/frees/images/favicons/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/frees/images/favicons/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/frees/images/favicons/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/frees/images/favicons/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/frees/images/favicons/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/frees/images/favicons/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/frees/images/favicons/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/frees/images/favicons/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="/frees/images/favicons/android-chrome-192x192.png" sizes="192x192">
		<link rel="icon" type="image/png" href="/frees/images/favicons/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/frees/images/favicons/favicon-16x16.png" sizes="16x16">
		<link rel="mask-icon" href="/frees/images/favicons/safari-pinned-tab.svg">

		<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

		<link href="/frees/css/bootstrap.css" rel="stylesheet"> 
		<link href="/frees/css/font-awesome.css" rel="stylesheet">
		<link href="/frees/css/simple-line-icons.css" rel="stylesheet">
		<link href="/frees/css/magnific-popup.css" rel="stylesheet">
		<link href="/frees/css/aos.css" rel="stylesheet">
		<link href="/frees/css/flexslider.css" rel="stylesheet">
		<link href="/frees/css/style.css" rel="stylesheet">
		<link href="/frees/css/colors/color-1.css" rel="stylesheet"> <!-- Color Scheme -->
 		
		<script src="/frees/js/modernizr.js"></script>

		<!--[if lt IE 9]>
		<script src="/frees/js/html5shiv.js"></script>
		<script src="/frees/js/respond.min.js"></script> 
		<![endif]-->

	</head>

<body>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W5ZXRVG"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<!-- Start Preloader -->
	<div id="page-preloader">
		<svg class="circular" height="50" width="50">
			<circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="3" stroke-miterlimit="10"></circle>
		</svg>
	</div>
	<!-- End Preloader -->

	<!-- Start Content -->
	<section id="main" class="wrapper">

		<!-- Start Header -->
		<header id="home" class="header-container">
			<div class="container-fluid header-content">

				<span class="trapeze-bg"></span>

				<div class="container screen-container">
					<div class="row">

						<div class="col-md-12 title-center-content">

							<div class="col-md-6 title-text">
								<h1>
									Инвестируем в бизнес, который начнет приносить доход онлайн
								</h1>
								<p>
								Хотите начать получать деньги не только оффлайн, а и онлайн?
								</p>
								<a href="#features-1" class="button hero-button smooth" title="Читать далее">Читать далее</a>

							</div>
						
							<div class="col-md-6 title-product">
								<span>Investment</span>
								<img src="/frees/images/first-image.png" alt="investment">
							</div>
						
						</div>

						<nav class="scroll-link">
							<a href="#features-1" class="smooth" title="Scroll">Scroll</a>
						</nav>

					</div>
				</div>

			</div>
		</header>
		<!-- End Header -->

		<!-- Start Navigation -->
		<nav id="navigation" class="navbar navbar-default navbar-top">
			<div class="container-fluid">

				<div class="nav-bg top-nav"></div>

				<div class="container">

					<div class="navbar-header">
						<button id="nav-icon" type="button" class="navbar-toggle collapsed nav-icon" data-toggle="collapse" data-target="#menu-navbar" aria-expanded="false">
								<span></span>
								<span></span>
								<span></span>
								<span></span>
						</button>
						<a class="navbar-brand smooth" href="#home">Amstery.<span class="logo-promo-text">Free</span></a> <!-- Title / Tagline -->
					</div>

					<div class="collapse navbar-collapse" id="menu-navbar">
						<ul class="nav navbar-nav navbar-right smooth-nav">
							<li class="current"><a href="/" title="Home">Home</a></li>
							<li><a href="/about" title="About">About</a></li>
							<li><a href="/contact" title="Products">Contact</a></li>
						</ul>
					</div>

					<div class="clearfix"></div>

				</div>
			</div>
		</nav>
		<!-- End Navigation -->

		<!-- Start Features 1 -->
		<section id="features-1" class="features block">
			<div class="container">
				<div class="row">

					<h2>Сделаем сайт для Вашего бизнеса бесплатно</h2>

					<div class="col-md-6 feature-img">
						<img src="/frees/images/second-image.png" alt="сощдание сайта">
					</div>

					<div class="col-md-6 feature-icons">						
						
						<div class="col-md-6 feature">
							<i class="icon-book-open"></i> <!-- Icon -->
							<h3>Brand-book</h3>
						</div>
						
						<div class="col-md-6 feature">
							<i class="icon-screen-smartphone"></i> <!-- Icon -->
							<h3>Адаптивный дизайн сайта</h3>
						</div>
						
						<div class="col-md-6 feature">
							<i class="icon-eyeglass"></i> <!-- Icon -->
							<h3>Трекинги</h3>
						</div>
						
						<div class="col-md-6 feature">
							<i class="icon-speedometer"></i> <!-- Icon -->
							<h3>Технические элементы для SEO</h3>
						</div>
						
						<div class="col-md-6 feature">
							<i class="icon-basket"></i> <!-- Icon -->
							<h3>Корзина</h3>
						</div>
						
						<div class="col-md-6 feature">
							<i class="icon-paper-plane"></i> <!-- Icon -->
							<h3>Онлайн-чат</h3>
						</div>
						
						<div class="col-md-6 feature">
							<i class="icon-screen-desktop"></i> <!-- Icon -->
							<h3>CRM, CMS</h3>
						</div>
						
						<div class="col-md-6 feature">
							<i class="icon-list"></i> <!-- Icon -->
							<h3>Форма для захвата контактов</h3>
						</div>
						

					</div> <!-- .feature-icons -->

				</div>
			</div> <!-- .container -->
		</section>
		<!-- End Features 1 -->

		<!-- Start Features 2 -->
		<section id="features-2" class="features block gray">
			<div class="container">
				<div class="row">

					<h2>Разработаем дизайн печатной продукции бесплатно</h2>

					<div class="col-md-4 feature-icons">
						
						<!-- Start Feature 1 -->
						<div class="col-md-12 feature feature-left">
							<i class="icon-picture"></i> <!-- Icon -->
							<h3>Визитки</h3>
						</div>
						<!-- End Feature 1 -->

						<!-- Start Feature 2 -->
						<div class="col-md-12 feature feature-left">
							<i class="icon-envelope-letter"></i> <!-- Icon -->
							<h3>Промо-раздатка</h3>
						</div>
						<!-- End Feature 2 -->

					</div> <!-- .feature-icons -->

					<div class="col-md-4 feature-img">
						<img src="/frees/images/third-image.jpg" alt="дизайн печатной продукции">
					</div>

					<div class="col-md-4 feature-icons">

						<!-- Start Feature 4 -->
						<div class="col-md-12 feature feature">
							<i class="icon-layers"></i> <!-- Icon -->
							<h3>Каталог услуг/товаров</h3>
						</div>
						<!-- End Feature 4 -->

						<!-- Start Feature 5 -->
						<div class="col-md-12 feature">
							<i class="icon-calendar"></i> <!-- Icon -->
							<h3>Календари</h3>
						</div>
						<!-- End Feature 5 -->

					</div> <!-- .feature-icons -->

				</div>
			</div> <!-- .container -->
		</section>
		<!-- End Features 2 -->

		<!-- Start Features 3 -->
		<section id="features-3" class="features block">
			<div class="container">
				<div class="row">

					<h2>Создадим и внедрим маркетинговую стратегию бесплатно</h2>

					<div class="col-md-6 feature-icons">
						
						<div class="col-md-6 feature feature-left">
							<i class="icon-user"></i> <!-- Icon -->
							<h3>Портрет клиента</h3>
						</div>
						
						<div class="col-md-6 feature feature-left">
							<i class="icon-directions"></i> <!-- Icon -->
							<h3>SEO продвижение</h3>
						</div>
						
						<div class="col-md-6 feature feature-left">
							<i class="icon-options"></i> <!-- Icon -->
							<h3>Ремаркетинг</h3>
						</div>		
						
						<div class="col-md-6 feature feature-left">
							<i class="icon-social-google"></i> <!-- Icon -->
							<h3>Контекстная реклама (Google, Yandex)</h3>
						</div>						
					
						<div class="col-md-6 feature feature-left">
							<i class="icon-envelope"></i> <!-- Icon -->
							<h3>Email, Viber и SMS рассылка</h3>
						</div>
						
						<div class="col-md-6 feature feature-left">
							<i class="icon-list"></i> <!-- Icon -->
							<h3>Контент-план и написание контента</h3>
						</div>
						
						<div class="col-md-6 feature feature-left">
							<i class="icon-social-instagram"></i> <!-- Icon -->
							<h3>SMM (Instagram, Facebook, Twitter, Pinterest)</h3>
						</div>		
						
						<div class="col-md-6 feature feature-left">
							<i class="icon-link"></i> <!-- Icon -->
							<h3>Баннерная реклама на релевантных площадках</h3>
						</div>		

					</div> <!-- .feature-icons -->

					<div class="col-md-6 feature-img">
						<img src="/frees/images/fouth-image.png" alt="маркетинг">
					</div>

				</div>
			</div> <!-- .container -->
		</section>
		<!-- End Features 3 -->

		<!-- Start Gallery -->
		<section id="gallery" class="gallery">
			<div class="container-fluid">
				<div class="container"> 
					<div class="row"> 

						<span class="trapeze-gallery-bg"></span>

						<div class="gallery-content">

							<h2>Всегда анализируем и оптимизируем бизнес для Вас</h2>

							<div class="gallery-tagline">
								<span>Be Pro</span>	
							</div>

							<ul class="list">
								<li><i class="fa fa-angle-right" aria-hidden="true"></i> Отчеты</li>
								<li><i class="fa fa-angle-right" aria-hidden="true"></i> Дашборды</li>
								<li><i class="fa fa-angle-right" aria-hidden="true"></i> Статистика</li>
							</ul>

						</div>

						<!-- Start Slider -->
						<div class="gallery-img">
							<div class="flexslider slider">

								<!-- Start Slides -->
								<ul class="slides">

									<!-- Start Slide 1 -->
									<li style="background: url(/frees/images/sisters.jpg) center">
										<div class="flex-caption">
											<h3>Beauty <span class="text-notice"><br>Market</span></h3>
											<p>онлайн магазин косметики</p>
										</div>
										<div class="slider-bg-overlay"></div><!-- Overlay -->
									</li>
									<!-- End Slide 1 -->

									<!-- Start Slide 2 -->
									<li style="background: url(/frees/images/oldmanemu.jpg) center">
										<div class="flex-caption">
											<h3>Men <br><span class="text-notice">equipment</span></h3>
											<p>Все для тюнинга внедорожников.</p>
										</div>
										<div class="slider-bg-overlay"></div><!-- Overlay -->
									</li>
									<!-- End Slide 2 -->

								</ul>
								<!-- End Slides -->

							</div>	
						</div>
						<!-- End Slider -->

					</div>
				</div>
			</div> <!-- .container-fluid -->
		</section>
		<!-- End Gallery -->

		<!-- Start Call To Action -->
		<section id="cta" class="cta block color">
			<div class="cta-parallax-1"></div>
			<div class="cta-parallax-2"></div>
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						
						<h2>Хотите начать привлекать новых клиентов уже сегодня?</h2>

						<a href="#newsletter" class="button action-button-cta" title="Buy This Template">Присоединиться</a>

					</div>

				</div>
			</div>
		</section>
		<!-- End Call To Action -->

		<!-- Start Newsletter Form -->
		<section id="newsletter" class="newsletter block">
			<div class="container">
				<div class="row">

					<h2>Заполните анкету и менеджер свяжется с Вами</h2>

					<!-- Start Form -->
					<div class="col-md-12 form-block">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="button action-button-cta"><?=Yii::t('app','Send Message')?></button>
                            </div>
                        </div>
                    </form>
						<span class="newsletter-notice">We don't like spam too</span>
					</div>
					<!-- End Form -->

				</div>
			</div> <!-- .container -->
		</section>
		<!-- End Newsletter Form -->

	</section>
	<!-- End Content -->

<!-- Include JS -->
<script src="/frees/js/jquery.min.js"></script>
<script src="/frees/js/bootstrap.min.js"></script>
<script src="/frees/js/device.min.js"></script>
<script src="/frees/js/retina.min.js"></script>
<script src="/frees/js/nav.js"></script>
<script src="/frees/js/smooth-scroll.min.js"></script>
<script src="/frees/js/parallax.js"></script>
<script src="/frees/js/aos.js"></script>
<script src="/frees/js/flexslider.js"></script>
<script src="/frees/js/youtubebackground.js"></script>
<script src="/frees/js/magnific-popup.min.js"></script>
<script src="/frees/js/init.js"></script>
<script src="/js/jqBootstrapValidation.js"></script>
<script src="/js/contact_me.js"></script>

<!--[if lte IE 9]>
<script src="/frees/js/placeholders.js"></script>
<script src="/frees/js/init-for-ie.js"></script>
<![endif]-->

</body>
</html>