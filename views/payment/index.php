<?php
/* @var $this yii\web\View */
?>
<section id="consulting" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-heading text-center">Request Consulting</h1>

	            <div class="text-center" style="margin-left: 30%">
		            <script src="https://api.fondy.eu/static_common/v1/checkout/ipsp.js"></script>
		            <script>
                        function checkoutInit(url) {
                            $ipsp('checkout').scope(function() {
                                this.setCheckoutWrapper('#checkout_wrapper');
                                this.addCallback(__DEFAULTCALLBACK__);
                                this.action('show', function(data) {
                                    $('#checkout_loader').remove();
                                    $('#checkout').show();
                                });
                                this.action('hide', function(data) {
                                    $('#checkout').hide();
                                });
                                this.action('resize', function(data) {
                                    $('#checkout_wrapper').width(480).height(data.height);
                                });
                                this.loadUrl(url);
                            });
                        };
                        var button = $ipsp.get("button");
                        button.setMerchantId(1433158);
                        button.setAmount(3200, 'RUB', true);
                        button.setHost('api.fondy.eu');
                        button.addField({
                            label: 'Name',
                            name: 'name',
                            required: true
                        });
                        checkoutInit(button.getUrl());
		            </script>

		            <div id="checkout">
			            <div id="checkout_wrapper"></div>
		            </div>
	            </div>
            </div>
        </div>
    </div>
</section>
