<?php
/* @var $this yii\web\View */
?>
<section id="consulting" class="bg-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="section-heading text-center">Request Consulting</h1>

	            <div class="text-center">
		            <h2>
			            <?php echo $text ?><br><br>
			            We will contact with you
		            </h2>
	            </div>
            </div>
        </div>
    </div>
</section>
