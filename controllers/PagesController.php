<?php

namespace app\controllers;

use app\models\Pages;
use Yii;

class PagesController extends BaseController
{
	public $description;
	
    public function actionIndex($action)
    {
		$page = Pages::find()->where(['url' => $action])->one();
		
		if ($page) { 
			if (!$page->layout) {
				$this->layout = false;
			}
			$this->view->title = $page->title;
			\app\components\Metatags::getMetatags($page->mt_id);
			
			if ($action == 'free') {
				return $this->render('free', ['page' => $page]);
			}
			
			return $this->render('index', ['page' => $page]);
		}
		else
			return $this->render('404');
    }

}
