<?php
namespace app\controllers;

use Yii;
use app\models\Seo;
use yii\web\Controller;
use yii\web\Session;
use yii\helpers\Url;

class BaseController extends Controller
{
    static public $body_class = '';
    static public $logo = '@web/img/dinarys.png';

    static public function getMeta(){
        $sql = "SELECT `route`,`title`,`description`,`keywords` FROM `seo` WHERE (`route`=:url) OR ((:url REGEXP `route`) AND (LEFT(`route`,1)='^'))";
        return $model = Yii::$app->db->createCommand($sql, [
            'url'=>Url::current(),
        ])->queryOne();
    }
    static public function isHome()
    {
        return Yii::$app->controller->id == 'site' && Yii::$app->controller->action->id == 'index';
    }
    static public function getTagFromDB($tag)
        {
            if(!is_string($tag) && strlen($tag) < 1)
                return false;
            $rows = (new \yii\db\Query())
            ->select('value_ru,value_en')
            ->from('settings')
            ->where(['key' => $tag])
            ->one();
            $dbValue = (Yii::$app->language == 'en-US') ? $rows['value_en'] : $rows['value_ru'];
            return $dbValue;
        }
}