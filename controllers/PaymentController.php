<?php

namespace app\controllers;

use app\models\Team;
use Yii;

class PaymentController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

	public function actionView($url)
	{
		$model = Team::find()->where(['url' => $url])->one();

		if ($model) {
            return $this->render('view', ['model' => $model]);
		} else {
			return $this->redirect('/team');
		}
	}

	public function actionSuccess()
	{

		$data = Yii::$app->request->post();

		file_put_contents('data.txt', json_encode($data));

		return $this->render('empty', [
			'text' => "Success"
		]);
	}

	public function actionError()
	{

		$data = Yii::$app->request->post();

		file_put_contents('data.txt', json_encode($data));

		return $this->render('empty', [
			'text' => "Error"
		]);
	}

	public function actionCallback()
	{
		$data = Yii::$app->request->post();

		file_put_contents('data.txt', json_encode($data));

		return $this->render('empty', [
			'text' => "success"
		]);
	}

}
