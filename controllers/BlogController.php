<?php

namespace app\controllers;

use app\models\Blog;
use \yii\data\Pagination;

class BlogController extends BaseController
{
    public function actionIndex()
    {
				$query = Blog::find();
				$countQuery = clone $query;
				$pages = new Pagination(['totalCount' => $countQuery->count()]);
				$model = $query->offset($pages->offset)
						->limit($pages->limit)
						->all();

				return $this->render('index', [
						 'model' => $model,
						 'pages' => $pages,
				]);
    }
	
		public function actionView($url)
		{
				$model = Blog::find()->where(['url' => $url])->one();
			
				if($model) {
					\app\components\Metatags::getMetatags($model->mt_id);
					return $this->render('view', ['model' => $model]);
				} else {
					return $this->render('404');
				}
		}

}
