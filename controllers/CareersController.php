<?php

namespace app\controllers;

use app\models\Careers;

class CareersController extends BaseController
{
    public function actionIndex()
    {
				$model = Careers::find()->all();
        return $this->render('index', ['model' => $model]);
    }
	
		public function actionView($url)
		{
				$model = Careers::find()->where(['url' => $url])->one();
			
				if ($model) { 
					return $this->render('view', ['model' => $model]);
				} else {
					return $this->redirect('/careers');
				}        
		}

}
