$('#menu_tabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
});

$('#blog-title').on('keyup', function(){
	var csrfToken = $('meta[name="csrf-token"]').attr("content");
	$.post('/admin/blog/generate-url?title='+$(this).val(), {_csrf : csrfToken}, function(data){
		$('#blog-url').val(data);
	});
});